<?php
include "../../../vendor/autoload.php";
use App\Pondit\Mobile\Mobile;

$obj = new Mobile();
$total_Item = $obj->num_of_row('mobile_models');

if (!empty($_GET['item_p_pg'])) {
    unset($_SESSION['item_p_pg']);
    $_SESSION['item_p_pg'] = $_GET['item_p_pg'];
} elseif (empty($_GET['item_p_pg']) && !empty($_SESSION['item_p_pg'])) {
    $_SESSION['item_p_pg'] = $_SESSION['item_p_pg'];
} else {
    $_SESSION['item_p_pg'] = 5;
}


if (isset($_GET['page'])) {
    unset($_SESSION['page']);
    $_SESSION['page'] = $_GET['page'];
} else {
    $_SESSION['page'] = 0;
}


if (empty($_GET['order']) or $_GET['order'] == '9-1') {
    $_SESSION['order'] = '9-1';
} else {
    $_SESSION['order'] = $_GET['order'];

}
$allData = $obj->setData($_SESSION)->index();

//echo "<pre>";
//print_r($allData);
?>
    <a href="create.php">Create</a> <a href="index.php?order=a-z"> A-Z </a><a href="index.php?order=9-1"> 9-1 </a>
<a href="trashList.php">See Deleted item</a>
    <form method="get" action="index.php">
        <select name="item_p_pg">
            <option value="5">Item per page</option>
            <option value="10">10</option>
            <option value="15">15</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="50">50</option>
        </select>
        <input type="submit" value="Go !"/>
        <?php
        if (isset($_SESSION['order']) && $_SESSION['order'] == '9-1') { ?>
            <input type="hidden" name="order" value="9-1"/>
        <?php } else { ?>
            <input type="hidden" name="order" value="a-z"/>
        <?php } ?>
    </form>

    <table border="1">
        <th>Serial</th>
        <th>Title</th>
        <th colspan="3">Action</th>
        <?php
        if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
            echo $_SESSION['Message'];
            unset($_SESSION['Message']);
        }
        $sl = ($_SESSION['item_p_pg'] * $_SESSION['page']) + 1;

        foreach ($allData as $oneData) {
            ?>
            <tr>
                <td><?php echo $sl++ ?></td>
                <td><?php echo $oneData['title'] ?></td>
                <td><a href="show.php?id=<?php echo $oneData['id'] ?>"">View</a></td>
                <td><a href="edit.php?id=<?php echo $oneData['id'] ?>"">Edit</a></td>
                <td><a href="trash.php?id=<?php echo $oneData['id'] ?>" onclick="return checkDelete()" "> Delete</a>
                </td>
            </tr>
        <?php } ?>
    </table>
    <script language="JavaScript" type="text/javascript">
        function checkDelete() {
            return confirm('Are you sure ?');
        }
    </script>

<?php
$total_page = ceil($total_Item / $_SESSION['item_p_pg']);
//echo $total_page;

for ($i = 0; $i < $total_page; $i++) {
    if (isset($_GET['order']) && $_GET['order'] == 'a-z') {
        echo '<a href="index.php?order=a-z&page=' . "$i" . '">' . "$i " . '</a>';
    } else {
        echo '<a href="index.php?page=' . "$i" . '">' . "$i " . '</a>';
    }


}
?>
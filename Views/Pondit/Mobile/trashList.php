<?php
include "../../../vendor/autoload.php";
use App\Pondit\Mobile\Mobile;

$obj = new Mobile();

$allData = $obj->trashlist();

?>
<a href="index.php">Back to list</a>
<table border="1">
    <th>Serial</th>
    <th>Title</th>
    <th colspan="3">Action</th>
    <?php
    if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
        echo $_SESSION['Message'];
        unset($_SESSION['Message']);
    }
    $sl = ($_SESSION['item_p_pg'] * $_SESSION['page']) + 1;

    foreach ($allData as $oneData) {
        ?>
        <tr>
            <td><?php echo $sl++ ?></td>
            <td><?php echo $oneData['title'] ?></td>
            <td><a href="restore.php?id=<?php echo $oneData['id'] ?>" onclick="return checkDelete()" "> Restore</a></td>
            <td><a href="delete.php?id=<?php echo $oneData['id'] ?>" onclick="return checkDelete()" "> Delete</a></td>
            </td>
        </tr>
    <?php } ?>
</table>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are you sure ?');
    }
</script>

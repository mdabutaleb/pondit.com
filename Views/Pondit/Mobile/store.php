<?php
include "../../../vendor/autoload.php";
use App\Pondit\Mobile\Mobile;

$obj = new Mobile();
//echo "<pre>";
//print_r($_POST);
//die();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['mobile_model']) && !empty($_POST['mobile_model'])) {
        if (preg_match("/([A-Za-z0-9_-])/", $_POST['mobile_model'])) {
            $_POST['mobile_model'] = filter_var($_POST['mobile_model'], FILTER_SANITIZE_STRING);
            $obj->setData($_POST)->store();
        } else {
            $_SESSION['Message'] = "<h3>Invalid Input</h3>";
            header('location:create.php');
        }
    } else {
        $_SESSION['Message'] = "<h3>Input can't be empty</h3>";
        header('location:create.php');
    }
    die();

} else {
    $_SESSION['Message'] = "<h3>Opps ! Something going wrong</h3>";
    header('location:create.php');
}

<?php
include "../../../vendor/autoload.php";
use App\Pondit\Mobile\Mobile;

$obj = new Mobile();

$oneData = $obj->setData($_GET)->show();
if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<a href="index.php">Back to list</a>
<fieldset>
    <legend>Edit mobile model</legend>
    <form action="update.php" method="post">
        <label>Edit model</label>
        <input type="text" name="mobile_model" value="<?php echo $oneData['title'] ?>"/>
        <input type="submit" value="Update"/>
        <input type="reset" value="Reset"/>
        <input type="hidden" name="id" value="<?php echo $oneData['id']?>"/>
    </form>
</fieldset>
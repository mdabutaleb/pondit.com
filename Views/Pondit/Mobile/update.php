<?php
include "../../../vendor/autoload.php";
use App\Pondit\Mobile\Mobile;

$obj = new Mobile();
$id = $_POST['id'];


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!empty($_POST['mobile_model'])) {
        if (preg_match("/([A-Za-z0-9-_])/", $_POST['mobile_model'])) {
            $_POST['mobile_model'] = filter_var($_POST['mobile_model'], FILTER_SANITIZE_STRING);
            $obj->setData($_POST)->update();
        } else {
            $_SESSION['Message'] = "<h3>Invalid input !</h3>";
            header("location:edit.php?id=$id");
        }
    } else {
        $_SESSION['Message'] = "<h3>Input can't be empty !</h3>";
        header("location:edit.php?id=$id");
    }

} else {
    $_SESSION['Message'] = "<h3>Opps something going wrong !</h3>";
    header('location:index.php');
}
